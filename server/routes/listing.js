// listing.js
/*
 * Serve JSON to our AngularJS client
 */
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/listingdemoapp');

var Schema = mongoose.Schema;

// create a schema
var listingSchema = new Schema({
    manufacturer: {
        make: {
            type: String,
            required: true
        }
    },
    model: {
        type: String,
        required: true,
    }
});

// the schema is useless so far
// we need to create a model using it
var Listing = mongoose.model('Listing', listingSchema);

console.log(Listing.find({}));

var express = require('express');
var router = express.Router();

/* GET /listing */

router.route('/')
    .get(function(req, res) {
        console.log('route list all triggered!!');
        Listing.find(function(err, listing) {
            console.log('found', listing);
            if (err) return next(err);
            res.json(listing);
        });
    })
    .post(function(req, res) {
        console.log(req.body);
        if (!req.body.data.manufacturer.make || !req.body.data.model)
            return res.send('make and model required!!');

        var newListing = new Listing(req.body.data);
        console.log('saving', newListing);
        // save the todo
        newListing.save(function(err) {
            if (err) {
                return res.send(err);
            }

            res.json({
                message: 'Listing added!',
                data: newListing
            });
        });
    });

router.route('/:id')
    .put(function(req, res, next) {
        var newListing = req.body.data;
        console.log('update', req.params.id, req.body);
        Listing.findOneAndUpdate({
            _id: req.params.id
        }, newListing, {
            upsert: true
        }, function(err, listing) {
            if (err) {
                // what is 500?!
                // 500 internal server error --> db failed to save
                return res.status(500).send({
                    error: err
                });
            }

            console.log('found Listing', newListing); //listing is before update

            res.json({
                message: 'Listing updated!',
                data: newListing
            });
        });
    })
    .delete(function(req, res, next) {
        console.log('delete', req.params.id);
        Listing.findOne({
            _id: req.params.id
        }, function(err, listing) {
            console.log('found', listing);
            if (err) {
                return res.status(500).send({
                    error: err
                });
            }

            res.json({
                message: 'Listing removed!',
                data: listing
            });

            listing.remove();
        });
    });

module.exports = router;