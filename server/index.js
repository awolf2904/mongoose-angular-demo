// index.js
var express = require('express'),
    morgan = require('morgan'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    listing = require('./routes/listing'),
    // auth = require('./routes/auth'),
    // api = require('./routes/api'),
    // todos = require('./routes/todos'),
    app = express(),
    server = require('http').Server(app),
    router = express.Router();

app.use(morgan('dev')); // log every request to the console
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());
app.use(methodOverride());

/**
 * Routes
 */

// app.use('/auth', auth.authRoute);
// app.use('/api', api);

// listing api
app.use('/listing', listing);

router.route('/').get(function(req, res) {
    //carData --> could be also added to db
    res.json([{
        make: "Audi",
        models: [{
            model: 'A1'
        }, {
            model: 'A2'
        }]
    }, {
        make: "BMW",
        models: [{
            model: '316i'
        }, {
            model: '318i'
        }]
    }]);
});

app.use('/formdata', router);

app.use(express.static(__dirname + '/../client'));

server.listen(8080, function() {
    console.log('Listening on port %d', server.address().port);
});