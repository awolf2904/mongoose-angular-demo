# What is this repository for? #
It's demo code for this [SO question](http://stackoverflow.com/questions/32384747/cant-submit-angularjs-cascading-dropdown-values-with-ng-options/32385158#comment52674768_32385158). 

# How do I get set up? #
Clone the repository and run `npm install` and then start the express server with `npm start`.
(Before you start you should have a running mongoserver started in another terminal `$ mongod`.

#License#
MIT