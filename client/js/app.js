// app.js

angular.module('demoApp', [])
    .factory('listingFactory', ListingFactory)
    .controller('mainController', MainController);

function MainController($scope, listingFactory) {
    /*$scope.carData = [{
        make: "Audi",
        models: [{
            model: 'A1'
        }, {
            model: 'A2'
        }]
    }, {
        make: "BMW",
        models: [{
            model: '316i'
        }, {
            model: '318i'
        }]
    }];*/

    listingFactory.getForm().then(function(formData) {
        $scope.carData = formData;
    });
    
    listingFactory.get().then(function(listings){
        $scope.listings = listings;
    });

    $scope.submit = function(formData) {
        var newData = angular.copy(formData); // copy because otherwise we would change the dropdown
        delete newData.manufacturer.models; // remove not needed prop.
        console.log('send this data to your backend', newData);
        listingFactory.save(newData).then(function(response){
            console.log(response);
            $scope.listings.push(response.listing.data);
        }); // error not handled yet
    };

    $scope.remove = function(listing) {
        var listings = $scope.listings;
        listingFactory.remove(listing).then(function() {
            listings.splice(listings.indexOf(listing), 1);
        }); // error not handled yet
    };
}

function ListingFactory($http) {
    var service = {
        get: function() {
            return $http.get('/listing').then(function(response) {
                return response.data;
            });
        },
        getForm: function() {
            return $http.get('/formdata').then(function(response) {
                return response.data;
            });
        },
        save: function(newListing) {
            return $http.post('/listing', {data: newListing}).then(function(response) {
                console.log(response);
                return {
                    message: 'listing saved',
                    listing: response.data
                };
            }, function(response) {
                return {
                    message: 'error occured',
                    data: response
                };
            });
        },
        remove: function(listing) {
            return $http.delete('/listing/' + listing._id).then(function(response) {
                return {
                    message: 'listing removed!',
                    listing: response.data
                };
            }, function(response) {
                return {
                    message: 'error occured',
                    data: response
                };
            })
        }
    };

    return service;
}